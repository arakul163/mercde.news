﻿using System;
using MvvmCross.Binding.Bindings.Target;
using UIKit;

namespace News.iOS.Bindings
{
	public class ViewAspectRatioBinding : MvxConvertingTargetBinding
	{
		private NSLayoutConstraint _aspectConstraint;

		public ViewAspectRatioBinding(UIView view)
			: base(view)
		{
		}

		protected override void SetValueImpl(object target, object value)
		{
			var targetView = (UIView)target;

			if (_aspectConstraint != null)
			{
				targetView.RemoveConstraint(_aspectConstraint);
				_aspectConstraint.Dispose();
			}

			_aspectConstraint = NSLayoutConstraint.Create(
				targetView,
				NSLayoutAttribute.Width,
				NSLayoutRelation.Equal,
				targetView,
				NSLayoutAttribute.Height,
				(float)value,
				0.0f	
			);
			targetView.AddConstraint(_aspectConstraint);
		}

		public override Type TargetType => typeof(float);
	}
}
