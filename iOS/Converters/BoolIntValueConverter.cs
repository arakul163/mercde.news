﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace News.iOS.Converters
{
	public class BoolIntValueConverter : MvxValueConverter<bool, int>
	{
		private readonly int _falseValue;
		private readonly int _trueValue;

		public BoolIntValueConverter()
			: this(default(int), default(int))
		{
		}

		public BoolIntValueConverter(int falseValue, int trueValue)
		{
			_falseValue = falseValue;
			_trueValue = trueValue;
		}

		protected override int Convert(bool value, Type targetType, object parameter, CultureInfo culture)
		{
			return value ? _trueValue : _falseValue;
		}
	}
}
