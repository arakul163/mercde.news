using MvvmCross.Platform.Plugins;

namespace News.iOS.Bootstrap
{
    public class NetworkPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.Network.PluginLoader, MvvmCross.Plugins.Network.iOS.Plugin>
    {
    }
}