﻿using Foundation;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.Platform;
using UIKit;

namespace News.iOS
{
	[Register("AppDelegate")]
	public class AppDelegate : MvxApplicationDelegate
	{
		public override UIWindow Window
		{
			get;
			set;
		}

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			SetupAppearance();

			var window = new UIWindow(UIScreen.MainScreen.Bounds);
			var setup = new Setup(this, window);
			setup.Initialize();

			var startup = Mvx.Resolve<IMvxAppStart>();
			startup.Start();

			Window = window;
			Window.MakeKeyAndVisible();

			return true;
		}

		private void SetupAppearance()
		{
			UINavigationBar.Appearance.TintColor = UIColor.FromRGB(246, 66, 26);
		}
	}
}

