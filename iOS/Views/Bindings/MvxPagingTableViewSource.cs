﻿using System.Windows.Input;
using Foundation;
using MvvmCross.Binding.ExtensionMethods;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace News.iOS.Views.Bindings
{
	public class MvxPagingTableViewSource : MvxSimpleTableViewSource
	{
		public MvxPagingTableViewSource(UITableView tableView, string nibName, string cellIdentifier = null, NSBundle bundle = null)
			: base(tableView, nibName, cellIdentifier, bundle)
		{
		}

		public ICommand LoadMoreItemsCommand
		{
			get;
			set;
		}

		public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
		{
			if (indexPath.Row != ItemsSource.Count() - 1)
				return;
			
			if (LoadMoreItemsCommand?.CanExecute(null) ?? false)
				LoadMoreItemsCommand.Execute(null);
		}
	}
}
