﻿using System;
using Foundation;
using SDWebImage;
using UIKit;

namespace News.iOS.Views
{
	[Register("ImageView")]
	public class ImageView : UIImageView
	{
		public ImageView(IntPtr handle) : base(handle)
		{
		}

		public string ImageUrl
		{
			get
			{
				return this.GetImage().AbsoluteString;
			}
			set
			{				
				this.CancelCurrentImageLoad();
				this.ResetCurrentImage();

				var url = value != null ? new NSUrl(value) : null;
				if (url != null)
				{
					this.SetImage(url);
				}
			}
		}

		private void ResetCurrentImage()
		{
			Image = null;
		}
	}
}
