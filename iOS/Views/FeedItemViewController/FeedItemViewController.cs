﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;

namespace News.iOS.Views.FeedItemViewController
{
	using iOS.Bindings;
	using ViewModels;

	public partial class FeedItemViewController : MvxViewController<FeedItemViewModel>
	{
		public FeedItemViewController() : base("FeedItemViewController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var bindingSet = this.CreateBindingSet<FeedItemViewController, FeedItemViewModel>();
			bindingSet.Bind(UsernameLabel).To(vm => vm.Username);
			bindingSet.Bind(TimestampLabel).To(vm => vm.Timestamp);
			bindingSet.Bind(DescriptionLabel).To(vm => vm.Description);
			bindingSet.Bind(ThumbnailImageView).For(v => v.ImageUrl).To(vm => vm.UserIconUrl);
			bindingSet.Bind(AttachmentImageView).For(v => v.ImageUrl).To(vm => vm.ImageUrl);
			bindingSet.Bind(AttachmentImageView).For(Target.AspectRatio).To(vm => vm.ImageAspect);
			bindingSet.Bind().For(v => v.Title).To(vm => vm.Title);
			bindingSet.Apply();
		}
	}
}

