// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace News.iOS.Views.FeedItemViewController
{
	[Register ("FeedItemViewController")]
	partial class FeedItemViewController
	{
		[Outlet]
		News.iOS.Views.ImageView AttachmentImageView { get; set; }

		[Outlet]
		UIKit.UILabel DescriptionLabel { get; set; }

		[Outlet]
		News.iOS.Views.ImageView ThumbnailImageView { get; set; }

		[Outlet]
		UIKit.UILabel TimestampLabel { get; set; }

		[Outlet]
		UIKit.UILabel UsernameLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AttachmentImageView != null) {
				AttachmentImageView.Dispose ();
				AttachmentImageView = null;
			}

			if (DescriptionLabel != null) {
				DescriptionLabel.Dispose ();
				DescriptionLabel = null;
			}

			if (ThumbnailImageView != null) {
				ThumbnailImageView.Dispose ();
				ThumbnailImageView = null;
			}

			if (TimestampLabel != null) {
				TimestampLabel.Dispose ();
				TimestampLabel = null;
			}

			if (UsernameLabel != null) {
				UsernameLabel.Dispose ();
				UsernameLabel = null;
			}
		}
	}
}
