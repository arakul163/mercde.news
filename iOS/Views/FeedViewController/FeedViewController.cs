﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using UIKit;

namespace News.iOS.Views.FeedViewController
{
	using Bindings;
	using ViewModels;

	public partial class FeedViewController : MvxViewController<FeedViewModel>
	{
		public FeedViewController() : base("FeedViewController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var refreshControl = new MvxUIRefreshControl();
			TableView.RefreshControl = refreshControl;

			var tableSource = new MvxPagingTableViewSource(TableView, "FeedTableViewCell");
			tableSource.DeselectAutomatically = true;
			tableSource.UseAnimations = true;
			TableView.Source = tableSource;

			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 374.0f;
			TableView.TableFooterView = new UIView();

			var bindingSet = this.CreateBindingSet<FeedViewController, FeedViewModel>();
			bindingSet.Bind(refreshControl).For(v => v.IsRefreshing).To(vm => vm.IsLoading);
			bindingSet.Bind(refreshControl).For(v => v.RefreshCommand).To(vm => vm.ReloadCommand);
			bindingSet.Bind(tableSource).For(v => v.ItemsSource).To(vm => vm.Items);
			bindingSet.Bind(tableSource).For(v => v.SelectionChangedCommand).To(vm => vm.ItemClickCommand);
			bindingSet.Bind(tableSource).For(v => v.LoadMoreItemsCommand).To(vm => vm.LoadMoreCommand);
			bindingSet.Bind().For(v => v.Title).To(vm => vm.Title);
			bindingSet.Apply();
		}
	}
}

