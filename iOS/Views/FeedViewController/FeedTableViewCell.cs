﻿using System;
using CoreGraphics;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using News.iOS.Bindings;
using News.ViewModels;
using UIKit;

namespace News.iOS.Views.FeedViewController
{
	public partial class FeedTableViewCell : MvxTableViewCell
	{
		protected FeedTableViewCell(IntPtr handle) : base(handle)
		{
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			ShadowView.Layer.ShadowColor = UIColor.Black.CGColor;
			ShadowView.Layer.ShadowOffset = new CGSize(0, 1);
			ShadowView.Layer.ShadowOpacity = 0.2f;
			ShadowView.Layer.ShadowRadius = 1.0f;

			var bindingSet = this.CreateBindingSet<FeedTableViewCell, FeedDataObject>();
			bindingSet.Bind(UsernameLabel).To(vm => vm.Username);
			bindingSet.Bind(TimestampLabel).To(vm => vm.Timestamp);
			bindingSet.Bind(DescriptionLabel).To(vm => vm.Description);
			bindingSet.Bind(ThumbnailImageView).For(v => v.ImageUrl).To(vm => vm.UserIconUrl);
			bindingSet.Bind(AttachmentImageView).For(v => v.ImageUrl).To(vm => vm.ImageUrl);
			bindingSet.Bind(AttachmentImageView).For(Target.AspectRatio).To(vm => vm.ImageAspect);
			bindingSet.Apply();
		}
	}
}
