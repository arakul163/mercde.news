﻿using System;
using Android.Widget;
using MvvmCross.Binding.Bindings.Target;
using Square.Picasso;

namespace News.Droid.Bindings
{
	public class ImageViewUrlBinding : MvxConvertingTargetBinding
	{
		public ImageViewUrlBinding(ImageView imageView)
			: base(imageView)
		{
		}

		protected override void SetValueImpl(object target, object value)
		{
			var imageView = (ImageView)target;
			var imageUrl = (string)value;

			Picasso.With(imageView.Context)
			       .CancelRequest(imageView);
			
			imageView.SetImageDrawable(null);

			Picasso.With(imageView.Context)
			       .Load(imageUrl)
			       .Into(imageView);
		}

		public override Type TargetType => typeof(string);
	}
}
