﻿using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace News.Droid.Views
{
	using ViewModels;

	[Activity(Label = "FeedActivity", Theme = "@style/AppTheme")]
	public class FeedActivity : MvxAppCompatActivity<FeedViewModel>
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.activity_feed);

			var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			SetSupportActionBar(toolbar);
		}
	}
}
