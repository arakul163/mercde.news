﻿using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace News.Droid.Views
{
	using Android.Views;
	using ViewModels;

	[Activity(Label = "FeedItemActivity", Theme = "@style/AppTheme")]
	public class FeedItemActivity : MvxAppCompatActivity<FeedItemViewModel>
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.activity_feeditem);

			var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			SetSupportActionBar(toolbar);

			SupportActionBar.SetDisplayShowHomeEnabled(true);
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case Android.Resource.Id.Home:
					Finish();
					return true;
			}
			return base.OnOptionsItemSelected(item);
		}
	}
}
