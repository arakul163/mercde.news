﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace News.Droid.Views.Bindings
{
	[Register("news.droid.views.bindings.AspectRatioFrameLayout")]
	public class AspectRatioFrameLayout : FrameLayout
	{
		public AspectRatioFrameLayout(Context context)
			: this(context, null)
		{
		}

		public AspectRatioFrameLayout(Context context, IAttributeSet attrs)
			: this(context, attrs, 0)
		{
		}

		public AspectRatioFrameLayout(Context context, IAttributeSet attrs, int defStyleAttr)
			: base(context, attrs, defStyleAttr)
		{
			TypedArray a = context.ObtainStyledAttributes(attrs, Resource.Styleable.AspectRatioFrameLayout);

			_aspectRatio = a.GetFraction(Resource.Styleable.AspectRatioFrameLayout_aspectRatio, 1, 1, 0.0f);

			a.Recycle();
		}

		private float _aspectRatio;
		public float AspectRatio
		{
			get { return _aspectRatio; }
			set
			{
				if (Math.Abs(_aspectRatio - value) < float.Epsilon)
					return;

				_aspectRatio = value;
				RequestLayout();
			}
		}

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{
			base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

			if (Math.Abs(_aspectRatio) > float.Epsilon)
			{
				var originalWidth = MeasuredWidth;
				var calculatedHeight = (int)(originalWidth / _aspectRatio);

				base.OnMeasure(MeasureSpec.MakeMeasureSpec(originalWidth, MeasureSpecMode.Exactly),
							   MeasureSpec.MakeMeasureSpec(calculatedHeight, MeasureSpecMode.Exactly));
			}
		}
	}
}
