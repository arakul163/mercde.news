﻿using System;
using System.Windows.Input;
using Android.Content;
using Android.Runtime;
using Android.Util;
using MvvmCross.Binding.Droid.Views;

namespace News.Droid.Views.Bindings
{
	[Register("news.droid.views.bindings.MvxPagingListView")]
	public class MvxPagingListView : MvxListView
	{
		public MvxPagingListView(Context context)
			: this(context, null)
		{
		}

		public MvxPagingListView(Context context, IAttributeSet attrs)
			: this(context, attrs, new MvxAdapter(context))
		{
		}

		public MvxPagingListView(Context context, IAttributeSet attrs, IMvxAdapter adapter)
			: base(context, attrs, adapter)
		{
			Scroll += OnScroll;
		}

		public ICommand LoadMoreItemsCommand
		{
			get;
			set;
		}

		private void OnScroll(object sender, ScrollEventArgs e)
		{
			if (e.FirstVisibleItem + e.VisibleItemCount != e.TotalItemCount)
				return;

			if (LoadMoreItemsCommand != null && LoadMoreItemsCommand.CanExecute(null))
			{
				Console.WriteLine("LOAD MORE");
				LoadMoreItemsCommand.Execute(null);
			}
		}
	}
}
