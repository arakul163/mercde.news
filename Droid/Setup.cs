using Android.Content;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Platform.Platform;
using News.Droid.Bindings;
using System.Collections.Generic;
using System.Reflection;

namespace News.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
			return new App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

		protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
		{
			registry.RegisterCustomBindingFactory<ImageView>("ImageUrl", imageView => new ImageViewUrlBinding(imageView));
			base.FillTargetFactories(registry);
		}

		protected override IEnumerable<Assembly> AndroidViewAssemblies => new[]
		{
			typeof(Views.Bindings.AspectRatioFrameLayout).Assembly,
			typeof(Android.Support.Design.Widget.CoordinatorLayout).Assembly,
			typeof(MvvmCross.Binding.Droid.Views.MvxListView).Assembly
		};

		protected override IDictionary<string, string> ViewNamespaceAbbreviations => new Dictionary<string, string>
		{
		};

		protected override IEnumerable<string> ViewNamespaces => new[]
		{
			"Android.Views",
			"Android.Widget",
			"Android.Webkit",
			"MvvmCross.Binding.Droid.Views",
			"MvvmCross.Droid.Support.V4"
		};
    }
}
