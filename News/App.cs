using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using News.Core.IoC;
using News.ViewModels;

namespace News
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
			CreatableTypes()
				.WithAttribute<ImplementationAttribute>()
				.AsInterfaces()
				.RegisterAsAttributed();

            RegisterAppStart<FeedViewModel>();
        }
    }
}
