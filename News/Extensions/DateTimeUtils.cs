﻿using System;

namespace News.Extensions
{
	public static class DateTimeUtils
	{
		public static DateTime FromUnixTime(int unixTimeStamp)
		{
			DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
			return dateTime;
		}
	}
}
