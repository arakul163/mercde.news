﻿using System;
using System.Threading.Tasks;
using MvvmCross.Plugins.Network.Rest;
using News.Core.IoC;
using News.Models;
using Newtonsoft.Json;

namespace News.Services.BackendClient
{
	[Implementation]
	public class BackendClient : IBackendClient
	{
		private readonly Uri _endpoint;
		private readonly IMvxJsonRestClient _restClient;

		public BackendClient(IMvxJsonRestClient restClient)
		{
			_endpoint = new Uri("https://api.vk.com");
			_restClient = restClient;
		}

		public Task<Wall> GetWallAsync(int offset)
		{
			var uriBuilder = new UriBuilder(_endpoint);
			uriBuilder.Path = "/method/wall.get";
			uriBuilder.Query = $"domain=mercdev&extended=1&offset={offset}&v=5.60";

			var completionSource = new TaskCompletionSource<Wall>();
			var request = new MvxRestRequest(uriBuilder.Uri);
			_restClient.MakeRequestFor<JsonResponse<Wall>>(request,
				response =>
				{
					completionSource.SetResult(response.Result.Data);
				},
				exception =>
				{
					completionSource.SetException(exception);
				});
			return completionSource.Task;
		}

		private class JsonResponse<T>
		{
			[JsonProperty("response")]
			public T Data { get; set; }
		}
	}
}
