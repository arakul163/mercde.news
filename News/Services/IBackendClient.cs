﻿using System.Threading.Tasks;

namespace News.Services
{
	using Models;

	public interface IBackendClient
	{
		Task<Wall> GetWallAsync(int offset);
	}
}
