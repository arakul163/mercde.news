﻿using System;

namespace News.Core.IoC
{
	public enum ImplementationScope
	{
		Instance,
		Singleton,
		LazySingleton
	}

	[AttributeUsage(AttributeTargets.Class)]
	public class ImplementationAttribute : Attribute
	{
		public ImplementationAttribute()
		{
			Scope = ImplementationScope.Instance;
		}

		public ImplementationScope Scope
		{
			get;
			set;
		}
	}
}
