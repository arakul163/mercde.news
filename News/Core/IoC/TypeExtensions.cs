﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;

namespace News.Core.IoC
{
	public static class TypeExtensions
	{
		public static void RegisterAsAttributed(this IEnumerable<MvxTypeExtensions.ServiceTypeAndImplementationTypePair> pairs)
		{
			foreach (var pair in pairs)
			{
				var implementationAttribute = pair.ImplementationType
												  .GetCustomAttributes(typeof(ImplementationAttribute), false)
				                                  .Cast<ImplementationAttribute>()
												  .First();
				foreach (var serviceType in pair.ServiceTypes)
				{
					switch (implementationAttribute.Scope)
					{
						case ImplementationScope.Instance:
							Mvx.RegisterType(serviceType, pair.ImplementationType);
							break;
						case ImplementationScope.Singleton:
							var instance = Mvx.IocConstruct(pair.ImplementationType);
							Mvx.RegisterSingleton(serviceType, instance);
							break;
						case ImplementationScope.LazySingleton:
							var creator = new MvxLazySingletonCreator(pair.ImplementationType);
							Mvx.RegisterSingleton(serviceType, new Func<object>(() => creator.Instance));
							break;
					}
				}
			}
		}
	}
}
