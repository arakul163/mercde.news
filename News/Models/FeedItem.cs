﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace News.Models
{
	public class FeedItem
	{
		[JsonProperty("from_id")]
		public int FromId { get; set; }

		[JsonProperty("date")]
		public int Date { get; set; }

		[JsonProperty("text")]
		public string Text { get; set; }

		[JsonProperty("attachments")]
		public List<Attachment> Attachments { get; set; }

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			if (Attachments == null)
				Attachments = new List<Attachment>();
		}
	}
}
