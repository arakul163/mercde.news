﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace News.Models
{
	public class Wall
	{
		[JsonProperty("items")]
		public List<FeedItem> Items { get; set; }

		[JsonProperty("groups")]
		public List<Group> Groups { get; set; }

		[JsonProperty("profiles")]
		public List<Profile> Profiles { get; set; }

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			if (Items == null)
				Items = new List<FeedItem>();

			if (Groups == null)
				Groups = new List<Group>();

			if (Profiles == null)
				Profiles = new List<Profile>();
		}
	}
}
