﻿using Newtonsoft.Json;

namespace News.Models
{
	public class Group
	{
		[JsonProperty("id")]
		public int Id { get; set; }
		
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("photo_100")]
		public string ThumbnailUrl { get; set; }
	}
}
