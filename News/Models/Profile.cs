﻿using System;
using Newtonsoft.Json;

namespace News.Models
{
	public class Profile
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("first_name")]
		public string FirstName { get; set; }

		[JsonProperty("last_name")]
		public string LastName { get; set; }

		[JsonProperty("photo_100")]
		public string ThumbnailUrl { get; set; }
	}
}
