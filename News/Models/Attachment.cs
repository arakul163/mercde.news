﻿using System;
using Newtonsoft.Json;

namespace News.Models
{
	public class Attachment
	{
		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("photo")]
		public PhotoAttachment Photo { get; set; }

		public class PhotoAttachment
		{
			[JsonProperty("width")]
			public int Width { get; set; }

			[JsonProperty("height")]
			public int Height { get; set; }

			[JsonProperty("photo_1280")]
			public string ImageUrl { get; set; }
		}
	}
}
