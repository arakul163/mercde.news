﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using PropertyChanged;

namespace News.ViewModels
{
	using Extensions;
	using Models;
	using Services;

	[ImplementPropertyChanged]
	public class FeedViewModel : MvxViewModel
	{
		private readonly IBackendClient _backend;
		private int _loadedItemsCount;

		public FeedViewModel(IBackendClient backend)
		{
			_backend = backend;

			Items = new MvxObservableCollection<FeedDataObject>();
			ReloadCommand = new MvxCommand(async () => await ReloadData(), () => !IsLoading && !IsLoadingMore);
			LoadMoreCommand = new MvxCommand(async () => await LoadMore(), () => !IsLoading && !IsLoadingMore);
			ItemClickCommand = new MvxCommand<FeedDataObject>(ItemClicked);
		}

		public override async void Start()
		{
			base.Start();

			await ReloadData();
		}

		private async Task ReloadData()
		{
			IsLoading = true;
			try
			{
				var items = await LoadItemsAsync(_backend, 0);
				var itemsList = items.ToList();

				_loadedItemsCount = itemsList.Count;
				Items.ReplaceWith(FilterFeedObjects(itemsList));
			}
			catch (Exception e)
			{
				LoadingError = e;
			}
			finally
			{
				IsLoading = false;
			}
		}

		private async Task LoadMore()
		{
			IsLoadingMore = true;
			try
			{
				var items = await LoadItemsAsync(_backend, _loadedItemsCount);
				var itemsList = items.ToList();
				if (itemsList.Count > 0)
				{
					_loadedItemsCount += itemsList.Count;
					Items.AddRange(FilterFeedObjects(itemsList));
				}
			}
			catch (Exception e)
			{
				LoadingError = e;
			}
			finally
			{
				IsLoadingMore = false;
			}
		}

		private void ItemClicked(FeedDataObject feedDataObject)
		{
			ShowViewModel<FeedItemViewModel>(feedDataObject);
		}

		private static async Task<IEnumerable<FeedDataObject>> LoadItemsAsync(IBackendClient backendClient, int offset)
		{
			var wallModel = await backendClient.GetWallAsync(offset).ConfigureAwait(false);
			return from item in wallModel.Items
				   select CreateFeedItem(item, wallModel.Profiles, wallModel.Groups);
		}

		private static IEnumerable<FeedDataObject> FilterFeedObjects(IEnumerable<FeedDataObject> items)
		{
			return from item in items
				   where !string.IsNullOrEmpty(item.Description)
				   select item;
		}

		private static FeedDataObject CreateFeedItem(FeedItem item, IEnumerable<Profile> profiles, IEnumerable<Group> groups)
		{
			var feedItem = new FeedDataObject
			{
				Timestamp = DateTimeUtils.FromUnixTime(item.Date).ToString("dd MMM HH:mm"),
				Description = item.Text
			};

			if (item.FromId > 0)
			{
				var profile = profiles.First(x => x.Id == item.FromId);
				feedItem.Username = $"{profile.FirstName} {profile.LastName}";
				feedItem.UserIconUrl = profile.ThumbnailUrl;
			}
			else
			{
				var group = groups.First(x => x.Id == -item.FromId);
				feedItem.Username = group.Name;
				feedItem.UserIconUrl = group.ThumbnailUrl;
			}

			var photoAttachments = from attachment in item.Attachments
								   where attachment.Type == "photo" && attachment.Photo.ImageUrl != null
								   select attachment;
			var firstPhotoAttachment = photoAttachments.FirstOrDefault();
			if (firstPhotoAttachment != null)
			{
				feedItem.ImageUrl = firstPhotoAttachment.Photo.ImageUrl;
				feedItem.ImageAspect = (float)firstPhotoAttachment.Photo.Width / firstPhotoAttachment.Photo.Height;
			}
			return feedItem;
		}

		public bool IsLoading { get; set; }

		public bool IsLoadingMore { get; set; }

		public string Title => "News";

		public MvxObservableCollection<FeedDataObject> Items { get; set; }

		public Exception LoadingError { get; set; }

		public ICommand ReloadCommand { get; }

		public ICommand LoadMoreCommand { get; }

		public ICommand ItemClickCommand { get; }
	}
}
