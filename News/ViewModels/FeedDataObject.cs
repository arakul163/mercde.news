﻿namespace News.ViewModels
{
	public class FeedDataObject
	{
		public string Username { get; internal set; }

		public string UserIconUrl { get; internal set; }

		public string Timestamp { get; internal set; }

		public string Description { get; internal set; }

		public string ImageUrl { get; internal set; }

		public float ImageAspect { get; internal set; }
	}
}
