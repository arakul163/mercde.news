﻿using MvvmCross.Core.ViewModels;
using PropertyChanged;

namespace News.ViewModels
{
	[ImplementPropertyChanged]
	public class FeedItemViewModel : MvxViewModel
	{
		public void Init(FeedDataObject feedDataObject)
		{
			Username 	= feedDataObject.Username;
			UserIconUrl = feedDataObject.UserIconUrl;
			Timestamp 	= feedDataObject.Timestamp;
			Description = feedDataObject.Description;
			ImageUrl 	= feedDataObject.ImageUrl;
			ImageAspect = feedDataObject.ImageAspect;
		}

		public string Title => "Post";

		public string Username { get; set; }

		public string UserIconUrl { get; set; }

		public string Timestamp { get; set; }

		public string Description { get; set; }

		public string ImageUrl { get; set; }

		public float ImageAspect { get; set; }
	}
}
